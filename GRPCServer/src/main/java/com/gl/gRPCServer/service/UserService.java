package com.gl.gRPCServer.service;

import com.gl.gRPCServer.User;
import com.gl.gRPCServer.userGrpc;
import io.grpc.stub.StreamObserver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UserService extends userGrpc.userImplBase {

    @Override
    public void login(User.loginMessage request, StreamObserver<User.APIResponse> responseObserver) {
        System.out.println("Inside Login method");
        String username=request.getUsername();
        String password=request.getPassword();
        storeDetails(username,password);

        User.APIResponse.Builder responce = User.APIResponse.newBuilder();
        if(username.equals(password)) {
            responce.setResponseMessage("SUCCESS");
            responce.setResponseCode(200);
        }
        else{
                responce.setResponseMessage("Invalid username and password");
                responce.setResponseCode(400);
            }

            responseObserver.onNext(responce.build());
            responseObserver.onCompleted();

        }


    @Override
    public void logout(User.empty request, StreamObserver<User.APIResponse> responseObserver) {
        super.logout(request, responseObserver);
    }

    public void storeDetails(String username, String password){
        String url = "jdbc:postgresql://localhost:5432/gldemo";
        String user="postgres";
        String Pass="ishuSRI@99";

        try{
            Class.forName("org.postgresql.Driver");
            Connection connection = DriverManager.getConnection(url, user, Pass);
            System.out.println("Connection established");
            String query = "Insert into userdb(name, password) values(?,?)";
            PreparedStatement stmt = connection.prepareStatement(query);
            stmt.setString(1, username);
            stmt.setString(2, password);
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

}
