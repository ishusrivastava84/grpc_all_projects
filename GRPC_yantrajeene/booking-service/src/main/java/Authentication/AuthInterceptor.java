package Authentication;
import io.grpc.*;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import java.nio.charset.Charset;

import static io.grpc.Metadata.ASCII_STRING_MARSHALLER;

public class AuthInterceptor implements ServerInterceptor {


    public final static String  signingKey = "loginkey for users";
    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
        Status status;
        String token = headers.get(Metadata.Key.of("Authorization",ASCII_STRING_MARSHALLER));
        if(!validateToken(token)){
            status = Status.UNAUTHENTICATED.withDescription("YOU ARE NOT AUTHORISED/TOKEN EXPIRED");
            call.close(status,headers);
            return new ServerCall.Listener<ReqT>() {
            };
        }
        return next.startCall(call,headers);
    }


    public static boolean validateToken(String token){
        if(token == null){
            return false;
        } else if (token.isEmpty()) {
            return false;
        }
        try {
            Claims claims = Jwts.parser().setSigningKey(signingKey.getBytes(Charset.forName("UTF-8"))).parseClaimsJws(token).getBody();
        }catch (Exception e){
            return false;
        }

        return true;

    }
}
