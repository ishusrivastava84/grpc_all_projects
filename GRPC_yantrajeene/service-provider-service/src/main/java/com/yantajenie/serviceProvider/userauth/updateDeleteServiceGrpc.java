package com.yantajenie.serviceProvider.userauth;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 *will be consumed by the user details and service provider services
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: updateDelete.proto")
public final class updateDeleteServiceGrpc {

  private updateDeleteServiceGrpc() {}

  public static final String SERVICE_NAME = "updateDeleteService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.yantajenie.serviceProvider.userauth.UpdateDelete.delRequest,
      com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse> getDeleteUserMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "deleteUser",
      requestType = com.yantajenie.serviceProvider.userauth.UpdateDelete.delRequest.class,
      responseType = com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.yantajenie.serviceProvider.userauth.UpdateDelete.delRequest,
      com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse> getDeleteUserMethod() {
    io.grpc.MethodDescriptor<com.yantajenie.serviceProvider.userauth.UpdateDelete.delRequest, com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse> getDeleteUserMethod;
    if ((getDeleteUserMethod = updateDeleteServiceGrpc.getDeleteUserMethod) == null) {
      synchronized (updateDeleteServiceGrpc.class) {
        if ((getDeleteUserMethod = updateDeleteServiceGrpc.getDeleteUserMethod) == null) {
          updateDeleteServiceGrpc.getDeleteUserMethod = getDeleteUserMethod = 
              io.grpc.MethodDescriptor.<com.yantajenie.serviceProvider.userauth.UpdateDelete.delRequest, com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "updateDeleteService", "deleteUser"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yantajenie.serviceProvider.userauth.UpdateDelete.delRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new updateDeleteServiceMethodDescriptorSupplier("deleteUser"))
                  .build();
          }
        }
     }
     return getDeleteUserMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.yantajenie.serviceProvider.userauth.UpdateDelete.updateRequest,
      com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse> getUpdatePasswordMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "updatePassword",
      requestType = com.yantajenie.serviceProvider.userauth.UpdateDelete.updateRequest.class,
      responseType = com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.yantajenie.serviceProvider.userauth.UpdateDelete.updateRequest,
      com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse> getUpdatePasswordMethod() {
    io.grpc.MethodDescriptor<com.yantajenie.serviceProvider.userauth.UpdateDelete.updateRequest, com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse> getUpdatePasswordMethod;
    if ((getUpdatePasswordMethod = updateDeleteServiceGrpc.getUpdatePasswordMethod) == null) {
      synchronized (updateDeleteServiceGrpc.class) {
        if ((getUpdatePasswordMethod = updateDeleteServiceGrpc.getUpdatePasswordMethod) == null) {
          updateDeleteServiceGrpc.getUpdatePasswordMethod = getUpdatePasswordMethod = 
              io.grpc.MethodDescriptor.<com.yantajenie.serviceProvider.userauth.UpdateDelete.updateRequest, com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "updateDeleteService", "updatePassword"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yantajenie.serviceProvider.userauth.UpdateDelete.updateRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new updateDeleteServiceMethodDescriptorSupplier("updatePassword"))
                  .build();
          }
        }
     }
     return getUpdatePasswordMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static updateDeleteServiceStub newStub(io.grpc.Channel channel) {
    return new updateDeleteServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static updateDeleteServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new updateDeleteServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static updateDeleteServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new updateDeleteServiceFutureStub(channel);
  }

  /**
   * <pre>
   *will be consumed by the user details and service provider services
   * </pre>
   */
  public static abstract class updateDeleteServiceImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     *delete user(end points for userdetails and spdetails services)
     * </pre>
     */
    public void deleteUser(com.yantajenie.serviceProvider.userauth.UpdateDelete.delRequest request,
        io.grpc.stub.StreamObserver<com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteUserMethod(), responseObserver);
    }

    /**
     * <pre>
     *updated password from the userdetails and service provider details
     * </pre>
     */
    public void updatePassword(com.yantajenie.serviceProvider.userauth.UpdateDelete.updateRequest request,
        io.grpc.stub.StreamObserver<com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdatePasswordMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getDeleteUserMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.yantajenie.serviceProvider.userauth.UpdateDelete.delRequest,
                com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse>(
                  this, METHODID_DELETE_USER)))
          .addMethod(
            getUpdatePasswordMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.yantajenie.serviceProvider.userauth.UpdateDelete.updateRequest,
                com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse>(
                  this, METHODID_UPDATE_PASSWORD)))
          .build();
    }
  }

  /**
   * <pre>
   *will be consumed by the user details and service provider services
   * </pre>
   */
  public static final class updateDeleteServiceStub extends io.grpc.stub.AbstractStub<updateDeleteServiceStub> {
    private updateDeleteServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private updateDeleteServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected updateDeleteServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new updateDeleteServiceStub(channel, callOptions);
    }

    /**
     * <pre>
     *delete user(end points for userdetails and spdetails services)
     * </pre>
     */
    public void deleteUser(com.yantajenie.serviceProvider.userauth.UpdateDelete.delRequest request,
        io.grpc.stub.StreamObserver<com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteUserMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     *updated password from the userdetails and service provider details
     * </pre>
     */
    public void updatePassword(com.yantajenie.serviceProvider.userauth.UpdateDelete.updateRequest request,
        io.grpc.stub.StreamObserver<com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdatePasswordMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   *will be consumed by the user details and service provider services
   * </pre>
   */
  public static final class updateDeleteServiceBlockingStub extends io.grpc.stub.AbstractStub<updateDeleteServiceBlockingStub> {
    private updateDeleteServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private updateDeleteServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected updateDeleteServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new updateDeleteServiceBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     *delete user(end points for userdetails and spdetails services)
     * </pre>
     */
    public com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse deleteUser(com.yantajenie.serviceProvider.userauth.UpdateDelete.delRequest request) {
      return blockingUnaryCall(
          getChannel(), getDeleteUserMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     *updated password from the userdetails and service provider details
     * </pre>
     */
    public com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse updatePassword(com.yantajenie.serviceProvider.userauth.UpdateDelete.updateRequest request) {
      return blockingUnaryCall(
          getChannel(), getUpdatePasswordMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   *will be consumed by the user details and service provider services
   * </pre>
   */
  public static final class updateDeleteServiceFutureStub extends io.grpc.stub.AbstractStub<updateDeleteServiceFutureStub> {
    private updateDeleteServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private updateDeleteServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected updateDeleteServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new updateDeleteServiceFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     *delete user(end points for userdetails and spdetails services)
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse> deleteUser(
        com.yantajenie.serviceProvider.userauth.UpdateDelete.delRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteUserMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     *updated password from the userdetails and service provider details
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse> updatePassword(
        com.yantajenie.serviceProvider.userauth.UpdateDelete.updateRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdatePasswordMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_DELETE_USER = 0;
  private static final int METHODID_UPDATE_PASSWORD = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final updateDeleteServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(updateDeleteServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_DELETE_USER:
          serviceImpl.deleteUser((com.yantajenie.serviceProvider.userauth.UpdateDelete.delRequest) request,
              (io.grpc.stub.StreamObserver<com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse>) responseObserver);
          break;
        case METHODID_UPDATE_PASSWORD:
          serviceImpl.updatePassword((com.yantajenie.serviceProvider.userauth.UpdateDelete.updateRequest) request,
              (io.grpc.stub.StreamObserver<com.yantajenie.serviceProvider.userauth.UpdateDelete.APIResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class updateDeleteServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    updateDeleteServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.yantajenie.serviceProvider.userauth.UpdateDelete.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("updateDeleteService");
    }
  }

  private static final class updateDeleteServiceFileDescriptorSupplier
      extends updateDeleteServiceBaseDescriptorSupplier {
    updateDeleteServiceFileDescriptorSupplier() {}
  }

  private static final class updateDeleteServiceMethodDescriptorSupplier
      extends updateDeleteServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    updateDeleteServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (updateDeleteServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new updateDeleteServiceFileDescriptorSupplier())
              .addMethod(getDeleteUserMethod())
              .addMethod(getUpdatePasswordMethod())
              .build();
        }
      }
    }
    return result;
  }
}
