package com.yantrajenie.authentication.services;

import com.yantrajenie.authentication.Authentication;
import com.yantrajenie.authentication.updateDelete.UpdateDelete;
import com.yantrajenie.authentication.updateDelete.updateDeleteServiceGrpc;
import io.grpc.stub.StreamObserver;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;

public class UpdateDeleteService extends updateDeleteServiceGrpc.updateDeleteServiceImplBase {

    //to delete the user permanently from the portal (server to server communication)
    @Override
    public void deleteUser(UpdateDelete.delRequest request, StreamObserver<UpdateDelete.APIResponse> responseObserver) {
        System.out.println("Inside the delete method");

        String email = request.getEmail();

        String query = "delete from registration where email=?";

        UpdateDelete.APIResponse.Builder response = UpdateDelete.APIResponse.newBuilder();

        Connection con = null;
        if(Validations.validateEmail(email)){
            try {
                Class.forName("org.postgresql.Driver");
                con = DatabaseConnection.connect();
                PreparedStatement stmnt = con.prepareStatement(query);
                stmnt.setString(1,email);
                int i = stmnt.executeUpdate();
                if(i==1){
                    response.setResMessage("User deleted successfully");
                    response.setResCode(200);
                }else {
                    response.setResMessage("User doesn't exist");
                    response.setResCode(400);
                }
            }catch (SQLException e){
                e.printStackTrace();
            }catch (ClassNotFoundException e){
                e.printStackTrace();
            }finally {
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }else if(Validations.validateEmail(email) == false){
            response.setResMessage("Invaild email");
            response.setResCode(400);
        }

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    //to update the password coming form the userdb and service providerdb (server to server communication)
    @Override
    public void updatePassword(UpdateDelete.updateRequest request, StreamObserver<UpdateDelete.APIResponse> responseObserver) {
        System.out.println("Inside update password method");

        String email = request.getEmail();
        String oldPassword = request.getOldPassword();
        Base64.Encoder encoder = Base64.getEncoder();
        byte[] encode = encoder.encode(oldPassword.getBytes());
        String encOldPass = new String(encode);
        String newPassword = request.getNewPassword();
        byte[] encode1 = encoder.encode(newPassword.getBytes());
        String encNewPass = new String(encode1);
        String confirmPassword = request.getConfirmPassword();

        String query1 = "select password from registration where email=? and password=?";
        String query2 = "update registration set password=? where email =?";

        UpdateDelete.APIResponse.Builder response = UpdateDelete.APIResponse.newBuilder();

        Connection connect = null;

        if(Validations.validateEmail(email) && Validations.validatePassword(oldPassword) && Validations.validatePassword(newPassword)){
            try{
                Class.forName("org.postgresql.Driver");
                connect = DatabaseConnection.connect();
                PreparedStatement stmnt = connect.prepareStatement(query1);
                stmnt.setString(1,email);
                stmnt.setString(2,encOldPass);
                ResultSet rs1 = stmnt.executeQuery();
                if(rs1.next()){
                    String value = rs1.getString(1);
                    System.out.println(value);
                    if(newPassword.equals(confirmPassword)){
                        PreparedStatement stmnt2 = connect.prepareStatement(query2);
                        stmnt2.setString(1,encNewPass);
                        stmnt2.setString(2,email);
                        int i = stmnt2.executeUpdate();
                        if(i==1){
                            response.setResMessage("Password updated successfully");
                            response.setResCode(200);
                            System.out.println("updated successfully");
                            System.out.println("data send to the reg db successfully");
                        }else {
                            response.setResMessage("User doesn't exist");
                            response.setResCode(500);
                        }

                    }else {
                        response.setResMessage("Password doesn't matches");
                        response.setResCode(400);
                    }
                }else {
                    response.setResMessage("User doesn't exist");
                    response.setResCode(400);
                }
            }catch (SQLException e){
                e.printStackTrace();
            }catch (ClassNotFoundException e){
                e.printStackTrace();
            }finally {
                try {
                    connect.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }else if(Validations.validateEmail(email)==false){
            response.setResMessage("Invalid email");
            response.setResCode(400);
        }else if(Validations.validatePassword(oldPassword) == false){
            response.setResMessage("Invalid Old password");
            response.setResCode(400);
        }else if(Validations.validatePassword(encNewPass)==false){
            response.setResMessage("Invalid New Password");
            response.setResCode(200);
        }

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }
}
