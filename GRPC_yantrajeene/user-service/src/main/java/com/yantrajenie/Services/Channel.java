package com.yantrajenie.Services;

import com.yantrajenie.authentication.updateDelete.UpdateDelete;
import com.yantrajenie.authentication.updateDelete.updateDeleteServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class Channel {
    

    public static void sendUpdatedPass(String newPass, String oldPass, String email) {
        System.out.println("Inside the send update pass data to registration db");

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",8095).usePlaintext().build();
        UpdateDelete.updateRequest upReq = UpdateDelete.updateRequest.newBuilder().setEmail(email).setNewPassword(newPass).setOldPassword(oldPass).build();
        updateDeleteServiceGrpc.updateDeleteServiceBlockingStub upStub = updateDeleteServiceGrpc.newBlockingStub(channel);
        UpdateDelete.APIResponse apiResponse = upStub.updatePassword(upReq);
        System.out.println("Response Message: "+apiResponse.getResMessage());
        System.out.println("Response Code: "+apiResponse.getResCode());

    }

    public static void sendDeleteReq(String email) {
        System.out.println("Inside send delete request to delete user");

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",8095).usePlaintext().build();
        UpdateDelete.delRequest delReq = UpdateDelete.delRequest.newBuilder().setEmail(email).build();
        updateDeleteServiceGrpc.updateDeleteServiceBlockingStub delStub =updateDeleteServiceGrpc.newBlockingStub(channel);
        UpdateDelete.APIResponse apiResponse = delStub.deleteUser(delReq);
        System.out.println("Response Message: "+apiResponse.getResMessage());
        System.out.println("Response Code: "+apiResponse.getResCode());


    }
}
