package com.yantrajenie.Services;
import com.yantrajenie.grpc.UserService;
import com.yantrajenie.grpc.userServiceGrpc;
import io.grpc.stub.StreamObserver;

import java.sql.*;
import java.util.Base64;

public class User extends userServiceGrpc.userServiceImplBase {

    //user will be added from registration
    @Override
    public void addUser(UserService.adduser request, StreamObserver<UserService.Response1> responseObserver) {
        int userId = request.getUserId();
        String first_name = request.getFirstName();
        String last_name = request.getLastName();
        String username = request.getUsername();
        String password = request.getPassword();
        String email = request.getEmail();
        String contactNo = request.getContactNo();
        UserService.Response1.Builder response1 = UserService.Response1.newBuilder();
        Connection conn = null;
        try {
            Class.forName("org.postgresql.Driver");
            conn = Database.connect();
            System.out.println("Connection established");
            String query = "insert into userdetails values (?,?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setInt(1, userId);
            stmt.setString(2, first_name);
            stmt.setString(3, last_name);
            stmt.setString(4, username);
            stmt.setString(5, password);
            stmt.setString(6, email);
            stmt.setString(7, contactNo);
            int i = stmt.executeUpdate();
            if (i == 1) {
                response1.setResMsg("User Added Successfully");
                response1.setResCode("200");
            } else {
                response1.setResMsg("Invalid user");
                response1.setResCode("400");
            }
            System.out.println("User Added Successfully");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        responseObserver.onNext(response1.build());
        responseObserver.onCompleted();

    }


    //getting details using user id from the user_details table
    @Override
    public void usdetails(UserService.userdetails request, StreamObserver<UserService.Response2> responseObserver) {
        System.out.println("Inside the us details method");
        int user_id = request.getUserId();
        String firstName = "";
        String lastName = "";
        String username = "";
        String password = "";
        String email = "";
        String contactNo = "";
        String flatNo = "";
        String streetName = "";
        String landMark = "";
        String city = "";
        String state = "";
        String pinCode = "";

        String query1 = "select * from userdetails where user_id = ?";
        String query2 = "select * from useraddress where user_id = ?";
        UserService.Response2.Builder response = UserService.Response2.newBuilder();
        Connection connection = null;
        try {
            connection = Database.connect();
            PreparedStatement stmnt1 = connection.prepareStatement(query1);
            PreparedStatement stmnt2 = connection.prepareStatement(query2);
            stmnt1.setInt(1, user_id);
            stmnt2.setInt(1, user_id);
            ResultSet resultSet = stmnt1.executeQuery();
            if (resultSet.next()) {
                user_id = resultSet.getInt(1);
                firstName = resultSet.getString(2);
                lastName = resultSet.getString(3);
                username = resultSet.getString(4);
                password = resultSet.getString(5);
                email = resultSet.getString(6);
                contactNo = resultSet.getString(7);
                response.setResCode("200");
            } else {
                response.setResCode("400 - User Not Exist");
                System.out.println("User Does Not Exist");
            }
            ResultSet resultSet1 = stmnt2.executeQuery();
            if (resultSet1.next()) {
                flatNo = resultSet1.getString(2);
                streetName = resultSet1.getString(3);
                landMark = resultSet1.getString(4);
                city = resultSet1.getString(5);
                state = resultSet1.getString(6);
                pinCode = resultSet1.getString(7);
                response.setResCode("200");
            } else {
                response.setResCode("400 - User Not Exist");
                System.out.println("User Does Not Exist");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        response.setUserId(user_id);
        response.setFirstName(firstName);
        response.setLastName(lastName);
        response.setUsername(username);
        response.setPassword(password);
        response.setEmail(email);
        response.setContactNo(contactNo);
        response.setFlatNo(flatNo);
        response.setStreetName(streetName);
        response.setLandMark(landMark);
        response.setCity(city);
        response.setState(state);
        response.setPinCode(pinCode);


        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    //user address details by user_id
    @Override
    public void usaddress(UserService.address request, StreamObserver<UserService.Response3> responseObserver) {
        int userId = request.getUserId();
        String flatNo = request.getFlatNo();
        String streetName = request.getStreetName();
        String landMark = request.getLandMark();
        String city = request.getCity();
        String state = request.getState();
        int pinCode = request.getPinCode();
        UserService.Response3.Builder response3 = UserService.Response3.newBuilder();
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.connect();
            System.out.println("Connection established");
            String query = "insert into useraddress values (?,?,?,?,?,?,?)";
            PreparedStatement stmnt = con.prepareStatement(query);
            stmnt.setInt(1, userId);
            stmnt.setString(2, flatNo);
            stmnt.setString(3, streetName);
            stmnt.setString(4, landMark);
            stmnt.setString(5, city);
            stmnt.setString(6, state);
            stmnt.setInt(7, pinCode);
            int i = stmnt.executeUpdate();
            if (i == 1) {
                response3.setResMsg("Address Added Successfully");
                response3.setResCode("200");
            } else {
                response3.setResMsg("Invalid...Try Again");
                response3.setResCode("400");
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        responseObserver.onNext(response3.build());
        responseObserver.onCompleted();

    }

    @Override
    public void usUpdateAdd(UserService.update_address request, StreamObserver<UserService.Response4> responseObserver) {
        System.out.println("Inside user services update ");
        int userId = request.getUserId();
        String flatNo = request.getFlatNo();
        String streetName = request.getStreetName();
        String landMark = request.getLandMark();
        String city = request.getCity();
        String state = request.getState();
        int pinCode = request.getPinCode();
        UserService.Response4.Builder response4 = UserService.Response4.newBuilder();

        String query1 = "update useraddress set flatno=?,streetname=?,landmark=?,city=?," +
                "state= ?,pincode=? where user_id =?;";
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.connect();
            System.out.println("Connection Established");
            PreparedStatement stmnt1 = con.prepareStatement(query1);
            stmnt1.setString(1, flatNo);
            stmnt1.setString(2, streetName);
            stmnt1.setString(3, landMark);
            stmnt1.setString(4, city);
            stmnt1.setString(5, state);
            stmnt1.setInt(6, pinCode);
            stmnt1.setInt(7, userId);
            int i = stmnt1.executeUpdate();
            if (i == 1) {
                response4.setResMsg("User Address updated");
                response4.setResCode("200");
            } else {
                response4.setResMsg("Invalid User Id");
                response4.setResCode("400");
            }

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        responseObserver.onNext(response4.build());
        responseObserver.onCompleted();

    }



    //delete user by user email
    @Override
    public void usdelete(UserService.deluser request, StreamObserver<UserService.Response5> responseObserver) {
        String email = request.getEmail();
        UserService.Response5.Builder response5 = UserService.Response5.newBuilder();
        if (email != "") {
            Connection con = null;
            try {
                Class.forName("org.postgresql.Driver");
                con = Database.connect();
                System.out.println("Connection Established");
                String query1 = "delete from useraddress where useraddress.user_id in " +
                        "(select user_id from userdetails where email = ?);";
                PreparedStatement stmt1 = con.prepareStatement(query1);
                stmt1.setString(1,request.getEmail());
                int i1 = stmt1.executeUpdate();
                String query = "delete from userdetails where email = ?";
                PreparedStatement stmt = con.prepareStatement(query);
                stmt.setString(1, request.getEmail());
                int i = stmt.executeUpdate();
                if(i1 == 1) {
                    if (i == 1) {
                        Channel.sendDeleteReq(email);
                        response5.setResMsg("User Deleted Successfully");
                        response5.setResCode("200");
                    }else{
                        response5.setResMsg("Invalid User Id");
                        response5.setResCode("400");
                    }
                }else {
                    response5.setResMsg("Invalid User Id");
                    response5.setResCode("400");
                }
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    con.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        responseObserver.onNext(response5.build());
        responseObserver.onCompleted();
    }

    //to check user is valid or not
    @Override
    public void usvalidate(UserService.validate request, StreamObserver<UserService.Response6> responseObserver) {
        System.out.println("Checking Valid User....");
        int userId = request.getUserId();
        UserService.Response6.Builder response6 = UserService.Response6.newBuilder();
        Connection conn = null;
        if (userId > 0) {
            try {
                Class.forName("org.postgresql.Driver");
                conn = Database.connect();
                System.out.println("Connection established");
                String query = "select * from userdetails where user_id = ?";
                PreparedStatement stmt = conn.prepareStatement(query);
                stmt.setInt(1, userId);
                ResultSet resultSet = stmt.executeQuery();
                if (resultSet.next()) {
                    response6.setResMsg("User Exists");
                } else {
                    response6.setResMsg("User does not exist");
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    conn.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

        } else {
            response6.setResMsg("Invalid User Id... Please Enter Valid User Id");
        }
        responseObserver.onNext(response6.build());
        responseObserver.onCompleted();
    }

    @Override
    public void updatePass(UserService.updatepassword request, StreamObserver<UserService.Response7> responseObserver) {
        System.out.println("Inside the update passsword method");
        String email = request.getEmail();
        String oldPassword = request.getOldPassword();
        Base64.Encoder encoder = Base64.getEncoder();
        byte[] encode = encoder.encode(oldPassword.getBytes());
        String oldPass = new String(encode);
        String newPassword = request.getNewPassword();
        byte[] encode1 = encoder.encode(newPassword.getBytes());
        String newPass = new String(encode1);
        String checkNewPassword = request.getCheckNewPassword();

        String query1 = "select password from userdetails where email=? and password=?";
        String query2 = "update userdetails set password=? where email =?";

        UserService.Response7.Builder response = UserService.Response7.newBuilder();

        Connection connect = null;

        try{
            Class.forName("org.postgresql.Driver");
            connect = Database.connect();
            PreparedStatement stmnt = connect.prepareStatement(query1);
            stmnt.setString(1,email);
            stmnt.setString(2,oldPassword);
            ResultSet rs1 = stmnt.executeQuery();
            if(rs1.next()){
                String value = rs1.getString(1);
                System.out.println(value);
                if(newPassword.equals(checkNewPassword)){
                    PreparedStatement stmnt2 = connect.prepareStatement(query2);
                    stmnt2.setString(1,newPassword);
                    stmnt2.setString(2,email);
                    int i = stmnt2.executeUpdate();
                    if(i==1){
                        //sneding to the registration db to update the password
                        Channel.sendUpdatedPass(email,newPass,oldPass);
                        response.setResMsg("Password updated successfully");
                        response.setResCode("200");
                        System.out.println("updated successfully");
                        System.out.println("data Updated successfully");
                    }else {
                        response.setResMsg("Something went wrong");
                        response.setResCode("500");
                    }

                }else {
                    response.setResMsg("Password doesn't matches");
                    response.setResCode("400");
                }
            }else {
                response.setResMsg("Invalid old password/ Email, please write a valid old password/Email");
                response.setResCode("400");
            }

        }catch (SQLException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }finally {
            try {
                connect.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();

    }
    //to update use details
    @Override
    public void usUpdateDetails(UserService.update_details request, StreamObserver<UserService.Response8> responseObserver) {
        System.out.println("Inside user details");
        int userId = request.getUserId();
        String firstName = request.getFirstName();
        String lastName = request.getLastName();
        String username = request.getUsername();
        String email = request.getEmail();
        String contact_no = request.getContactNo();

        UserService.Response8.Builder response8 = UserService.Response8.newBuilder();

        String query = "update userdetails set first_name=?,last_name=?,username=?,email=?,contact_no=?" +
                "where user_id = ?;";
        Connection con = null;
        try{
            Class.forName("org.postgresql.Driver");
            con = Database.connect();
            System.out.println("Connection Established");
            PreparedStatement stmnt = con.prepareStatement(query);
            stmnt.setString(1,firstName);
            stmnt.setString(2,lastName);
            stmnt.setString(3,username);
            stmnt.setString(4,email);
            stmnt.setString(5,contact_no);
            stmnt.setInt(6,userId);

            int i = stmnt.executeUpdate();
            if(i == 1){
                response8.setResMsg("User Details Updated Successfully");
                response8.setResCode("200");
            }else {
                response8.setResMsg("Invalid User Id");
                response8.setResCode("400");
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            try {
                con.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        responseObserver.onNext(response8.build());
        responseObserver.onCompleted();
    }

}




