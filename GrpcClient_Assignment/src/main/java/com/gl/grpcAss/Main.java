package com.gl.grpcAss;

import com.gl.grpcAss.client.ClinetService;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import javax.management.MBeanServerBuilder;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {

        System.out.println("Starting client Service to get the token");
        Server server= ServerBuilder.forPort(9090).addService(new ClinetService()).build();

        server.start();
        System.out.println("Server is running on port no: "+server.getPort());
        server.awaitTermination();
    }
}