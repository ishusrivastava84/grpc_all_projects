package com.gl.grpcAssign;

import com.gl.grpcAssign.service.TokenService;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("Creating a channel b/w user and client");

        System.out.println("Starting client Service to get the token");
        Server server= ServerBuilder.forPort(8090).addService(new TokenService()).build();

        server.start();
        System.out.println("Server is running on port no: "+server.getPort());
        server.awaitTermination();
    }
}