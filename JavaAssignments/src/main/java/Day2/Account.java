package Day2;

public class Account {

    private double balance;
    //constructor for account
    public Account(double balance){
        this.balance = balance;
    }


    //balance getter
    public double getBalance() {
        return balance;
    }

    //function for withdrawing

    public void withdraw(double amount){

        this.balance = this.balance  - amount;
    }



    //function for deposit
    public void deposit(double amount){
        this.balance = this.balance  + amount;

    }



}
