package Day2.Task3;

public class Vehicle {

    private int regNo;
    private String brand;
    private double mileage;

    public int getRegNo() {
        return regNo;
    }

    public String getBrand() {
        return brand;
    }

    private double price;

    public double getMileage() {
        return mileage;
    }

    //constructor for vehicle class
    public Vehicle(int regNo, String brand, double mileage, double price) {
        this.brand = brand;
        this.mileage = mileage;
        this.regNo = regNo;
        this.price = price;
    }

    //display function for vehicle
    public void displayVehicleDetails() {
        System.out.println("{");
        System.out.println("Registration Number:" + this.regNo);
        System.out.println("Brand:" + this.brand);
        System.out.println("Mileage:" + this.mileage);
        System.out.println("Price:" + this.price);
        System.out.println("}");
    }


    //getter for price of vehicle
    public double getPrice() {
        return price;
    }
}
