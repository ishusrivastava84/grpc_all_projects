package Day3.Assignment.A1;

public class Action extends Movie{
    //setting the default latefee

    private double totalLateFee;


    public Action(String mpaaRating, int id, String title){
        super(mpaaRating,id,title);
    }

    public double getLateFee() {
        return totalLateFee;
    }

    public void setLateFee(double lateFee) {
        this.totalLateFee = lateFee;
    }

    private final double LATEFEECHARGES = 3.00;

    public double calcLateFee(int days){
        this.totalLateFee = LATEFEECHARGES*days;
        return this.totalLateFee;
    }
}
