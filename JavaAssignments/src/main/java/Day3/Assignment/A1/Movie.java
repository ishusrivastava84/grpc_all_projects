package Day3.Assignment.A1;

public class Movie {
        private String mpaaRating;
        private int Id;
        private String title;


    public Movie(String mpaaRating, int id, String title) {
        this.mpaaRating = mpaaRating;
        Id = id;
        this.title = title;
    }

    public String getMpaaRating() {
        return mpaaRating;
    }

    public void setMpaaRating(String mpaaRating) {
        this.mpaaRating = mpaaRating;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Movie)){
            return false;
        }
        Movie movie = (Movie) obj;
        return this.Id == movie.getId();
    }
}
