package Day3.Assignment.A2;

public class BookPlayground {
    public static void main(String[] args) {

        //creating magazine and novel class
        Magazine mag = new Magazine("AU189273","RootCause",80.89,"Comedy");
        Novel novel = new Novel("AU712713","Showmaster",90.00,"Arthur");

    //printing details of books
        mag.printDetails();
        novel.printDetails();
    }
}
