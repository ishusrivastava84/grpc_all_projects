package Day3.Assignment.A4;

public class Employee {

    private int empNo;
    private String empName;
    private String address;
    private String contactNo;

    public Employee(int empNo, String empName, String address, String contactNo) {
        this.empNo = empNo;
        this.empName = empName;
        this.address = address;
        this.contactNo = contactNo;
    }

    public int getEmpNo() {
        return empNo;
    }

    public void setEmpNo(int empNo) {
        this.empNo = empNo;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{\n");
        sb.append("Employee Number:"+ this.empNo+"\n");
        sb.append("Employee Name:"+this.empName+"\n");
        sb.append("Employee Address:"+this.address+"\n");
        sb.append("Employee Contact Number:"+this.contactNo+"\n}");
        return sb.toString();
    }
}
