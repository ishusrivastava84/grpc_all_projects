package Day3.Assignment.A4;

public class EmployeePlayground {
    public static void main(String[] args) {

        //creating 5 employees
        Employee e1 = new Employee(2,"Nikhil","noida","92831873");
        Employee e2 = new Employee(3,"Richa","Patna","189273");
        Employee e3 = new Employee(4,"amit","Lucknow","1928309");
        Employee e4 = new Employee(5,"Kriti","Noida","76873618");
        Employee e5 = new Employee(6,"Pankaj","Delhi","9890898");

        //creating 2 managers

        Manager m1 = new Manager(7,"srinidhi","patiala","18253681","Business",5);
        Manager m2 = new Manager(8,"Himanshu","hisar","78126378612","Engineering",6);


        //displaying employee details
        System.out.println(e1.toString());
        System.out.println(e2.toString());
        System.out.println(e3.toString());
        System.out.println(e4.toString());
        System.out.println(e5.toString());

        //displaying manager details
        System.out.println(m1.toString());
        System.out.println(m2.toString());



    }
}
