package Day3.Assignment.A5;

public class BranchOffices extends Company{

    private String branchId;
    private String location;
    private String department;


    public BranchOffices(String companyId, String name, String ho, String ceo, String branchId, String location, String department) {
        super(companyId, name, ho, ceo);
        this.branchId = branchId;
        this.location = location;
        this.department = department;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        sb.append("Company ID:"+this.getCompanyId()+"\n");
        sb.append("Company Name:"+this.getName()+"\n");
        sb.append("Company HO:"+this.getHo()+"\n");
        sb.append("Company CEO:"+this.getCeo()+"\n");
        sb.append("Branch ID:"+this.branchId+"\n");
        sb.append("Branch Address:"+this.getLocation()+"\n");
        sb.append("Branch Department:"+this.department+"\n}");
        return sb.toString();
    }
}
