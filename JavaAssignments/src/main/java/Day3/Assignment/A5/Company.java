package Day3.Assignment.A5;

public class Company {

    private String companyId;
    private String name;
    private String Ho;
    private String ceo;


    public Company(String companyId, String name, String ho, String ceo) {
        this.companyId = companyId;
        this.name = name;
        Ho = ho;
        this.ceo = ceo;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHo() {
        return Ho;
    }

    public void setHo(String ho) {
        Ho = ho;
    }

    public String getCeo() {
        return ceo;
    }

    public void setCeo(String ceo) {
        this.ceo = ceo;
    }
}
