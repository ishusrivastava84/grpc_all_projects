package Day3.Assignment.A5;

public class CompanyPlayground {
    public static void main(String[] args) {
        //creation of 5 branches of same company
        BranchOffices br1 = new BranchOffices("1234","GL","Stalber","Jamison","892","california","sales");
        BranchOffices br2 = new BranchOffices("1234","GL","Stalber","Jamison","298","texas","engineering");
        BranchOffices br3 = new BranchOffices("1234","GL","Stalber","Jamison","976","east texas","sales");
        BranchOffices br4 = new BranchOffices("1234","GL","Stalber","Jamison","099","north carolina","engineering");
        BranchOffices br5 = new BranchOffices("1234","GL","Stalber","Jamison","837","Delhi","research");

        //printing branch details
        System.out.println(br1.toString());
        System.out.println(br2.toString());
        System.out.println(br3.toString());
        System.out.println(br4.toString());
        System.out.println(br5.toString());

    }
}
