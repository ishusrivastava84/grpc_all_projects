package Day3.Assignment.A6;

public class Accounts {

    private double balance;

    public Accounts(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void deposit(double amount){
        this.balance= this.balance + amount;
    }

    public void withdraw(double amount){
        this.balance = this.balance - amount;
    }
}
