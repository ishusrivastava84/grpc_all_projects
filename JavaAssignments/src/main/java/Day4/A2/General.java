package Day4.A2;

public class General extends Compartment{

    @Override
    public String notice() {
        return "This is a general compartment";
    }
}
