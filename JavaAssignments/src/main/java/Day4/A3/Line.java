package Day4.A3;

public class Line extends Shape{

    private int pointOne;
    private int pointTwo;

    public Line(int x, int y, int pointOne, int pointTwo) {
        super(x, y);
        this.pointOne = pointOne;
        this.pointTwo = pointTwo;
    }

    public int getPointOne() {
        return pointOne;
    }

    public void setPointOne(int pointOne) {
        this.pointOne = pointOne;
    }

    public int getPointTwo() {
        return pointTwo;
    }

    public void setPointTwo(int pointTwo) {
        this.pointTwo = pointTwo;
    }

    @Override
    public String toString() {
        return this.pointOne+" , "+this.pointTwo;
    }

    @Override
    public void show() {

        System.out.println("Showing Line at location ("+super.toString()+") with two points at "+this.toString());

    }
}
