package Day4.A3;

import java.util.ArrayList;
import java.util.List;

public class ShapePlayground {
    public static void main(String[] args) {
        List<Shape> shapes = new ArrayList<>();
        shapes.add(new Line(2,3,2,1));
        shapes.add(new Rectangle(34,46,12,13));
        shapes.add(new Circle(12,12,10.0f));
        for (Shape s:shapes
             ) {
            s.show();
        }
    }

}
