package Day5;

public class AnimalPlayGround {

    public static void main(String[] args) {

        //creating fish class to demo functions
        Animal fish = new Fish(0,"nemo");
        fish.eat();
        fish.walk();

        //creating cat class to demo function
        Animal cat = new Cat(4,"garfield");
        cat.walk();
        cat.eat();


        //creating spider class to demo function
        Animal spider = new Spider(8);
        spider.eat();
        spider.walk();
    }
}
