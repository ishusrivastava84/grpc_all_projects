package Day5;

public class Fish extends Animal implements Pet{

    private String name;

    public Fish(int legs, String name) {
        super(legs);
        this.name = name;
    }

    public Fish(int legs) {
        super(legs);
    }

    @Override
    public void walk() {
        System.out.println("Fish is swimming");
    }

    @Override
    public void eat() {
        System.out.println("Fish is eating");
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public void play(){
        System.out.println("Fish is playing");
    }
}
