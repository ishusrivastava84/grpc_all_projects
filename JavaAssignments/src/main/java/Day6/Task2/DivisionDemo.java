package Day6.Task2;

public class DivisionDemo {
    public static void main(String[] args) {
        Division div = new Division(5,0);


        try {
            System.out.println(div.divide());
        }catch (InvalidDivisor e){
            System.out.println(e);

        }

        //2nd division demo with right input
        Division div1 = new Division(30,2);
        try {
            System.out.println(div1.divide());
        }catch (InvalidDivisor e){
            System.out.println(e);
        }
    }
}
