package Day6.Task2;

public class InvalidDivisor extends Exception{


    //class extending Exception
    //constructor with message for exception
    public InvalidDivisor(String message) {
        super(message);
    }
}
