package Day7.A1;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


//used in class
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@interface DBParams {

    //data with default value;
    String dbName() default "";
    String userId() default "user";
    String password() default "Password";
}
