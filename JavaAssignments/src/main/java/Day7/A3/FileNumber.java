package Day7.A3;

import java.io.*;
import java.util.Random;

public class FileNumber {
    public static void main(String[] args) {
        try {
            //filewriter class created
            FileWriter fw = new FileWriter(new File("Numbers.txt"));

            Random random = new Random();
            //writng data in file
            for(int i = 0; i < 30;i++){
                fw.write(random.nextInt(31)+"\n");
            }

            fw.close();

            //reading the same file for further processing
            FileReader fr = new FileReader("Numbers.txt");
            BufferedReader br = new BufferedReader(fr);
            int sum = 0;
            while (br.ready()){

                //reading eachfile
                sum = sum+Integer.parseInt(br.readLine());
            }
            System.out.println("Sum of Numbers are:"+sum);
            System.out.println("Average of Numbers are:"+sum/30);

        }catch (FileNotFoundException f){
            System.out.println("File Not Found");
        }
        catch (IOException f){
            System.out.println("Can't Create File/Invalid Input");
        }
    }
}
