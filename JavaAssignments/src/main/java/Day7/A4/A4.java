package Day7.A4;

import java.io.*;
import java.util.Arrays;
import java.util.Objects;

public class A4 {

    public static void main(String[] args) {
        try{
            File file = new File("Input.txt");
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            File file2 = new File("Output.txt");
            FileWriter fw = new FileWriter(file2);

            while(br.ready()){
                String line = br.readLine();

                //splitting the line in arrays
                String[] s = line.split("\\s");
                StringBuilder sb = new StringBuilder();

                //for each blank space in this splitted we will not appnd to string builder
                Arrays.stream(s).forEach(e-> {
                    if(!Objects.equals(e, "")){
                    sb.append(e).append(" ");
                    };});

                //wrtiting trimmed line to file
                fw.write(sb.toString()+"\n");
            }

            fr.close();
            fw.close();



        }catch (FileNotFoundException e){
            System.out.println("Enter a valid file path/name");
        }catch (IOException e){
            System.out.println("Error reading file");
        }
    }


}

