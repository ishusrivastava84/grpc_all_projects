package Day7.A5;

import java.util.Date;
class eg
{
    //changing arguements to right one in the main function
    public static void main(String[] args)
    {
        Date date = new Date();
        //adding deprecated annotation to suppress warning
        @Deprecated
        int year = date.getYear();
        System.out.println("The year is : "+year);
    }
}
