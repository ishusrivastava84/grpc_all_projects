import Day1.Factorial;
import Day1.NameOfMonths;
import Day1.PositiveNegativeA1;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class Day1Test {




    //testing negative check function
    @Test
    public void checkNegative(){
        assertEquals(true, PositiveNegativeA1.posOrNeg(7));
        assertEquals(false,PositiveNegativeA1.posOrNeg(-12));
    }



    //testing name of month in words function
    @Test
    public void monthName(){
        assertEquals("November", NameOfMonths.nameOfMonths(11));
        assertEquals("Invalid Month",NameOfMonths.nameOfMonths(13));
    }


    //testing factorial calculation
    @Test
    public void factorial(){
        assertEquals(120, Factorial.factorial(5));
        assertEquals(720,Factorial.factorial(6));
        assertNotEquals(234,Factorial.factorial(4));
    }

}
