package Day3;

import Day3.Assignment.A1.Action;
import Day3.Assignment.A1.Comedy;
import Day3.Assignment.A1.Drama;
import Day3.Assignment.A1.Mpaa;
import com.beust.ah.A;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class Assignment1Test {




    @Test
    public void constructorCheckComedy(){
        Comedy comedy = new Comedy(Mpaa.G.name(), 29,"Laughing Movie");
        assertEquals(Mpaa.G.name(),comedy.getMpaaRating());
        assertEquals(29,comedy.getId());
        assertEquals("Laughing Movie",comedy.getTitle());
    }

    //latefee function check for comedy

    @Test
    public void lateFeeCheckComedy(){
        Comedy comedy = new Comedy(Mpaa.G.name(), 29,"Laughing Movie");
        assertEquals(4.00,comedy.calcLateFee(2),0);
        assertNotEquals(6.00,comedy.calcLateFee(8));

    }


    @Test
    public void constructorCheckAction(){
        Action action = new Action(Mpaa.G.name(), 29,"Laughing Movie");
        assertEquals(Mpaa.G.name(),action.getMpaaRating());
        assertEquals(29,action.getId());
        assertEquals("Laughing Movie",action.getTitle());
    }

    //latefee function check for Action

    @Test
    public void lateFeeCheckAction(){
        Action action = new Action(Mpaa.G.name(), 29,"Laughing Movie");
        assertEquals(6.00,action.calcLateFee(2),0);
        assertNotEquals(12.00,action.calcLateFee(8));

    }




    @Test
    public void constructorCheckDrama(){
        Drama drama = new Drama(Mpaa.G.name(), 29,"Laughing Movie");
        assertEquals(Mpaa.G.name(),drama.getMpaaRating());
        assertEquals(29,drama.getId());
        assertEquals("Laughing Movie",drama.getTitle());
    }

    //latefee function check for Drama

    @Test
    public void lateFeeCheckDrama(){
        Drama drama = new Drama(Mpaa.G.name(), 29,"Laughing Movie");
        assertEquals(5.00,drama.calcLateFee(2),0);
        assertNotEquals(6.00,
                drama.calcLateFee(8));

    }
}
