package Day3;

import Day3.Assignment.A2.Magazine;
import Day3.Assignment.A2.Novel;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class Assignment2Test {


    //Constructor test for magazine
    @Test
    public void magazineConstrutorTest(){
        Magazine mag = new Magazine("AI129013","Two Theives",765.88,"Adventure");
        assertEquals("AI129013",mag.getIsbn());
        assertEquals("Two Theives",mag.getTitle());
        assertEquals("Adventure",mag.getType());
        assertEquals(765.88,mag.getPrice(),0);

    }

    //Constructor test for novel

    public void novelConstructorTest(){
        Novel novel = new Novel("B1179","Boathouse",876.99,"Sherlock holmes");
        assertEquals("B1179",novel.getIsbn());
        assertEquals("Boathouse",novel.getTitle());
        assertEquals(876.99,novel.getPrice(),0);
        assertEquals("Sherlock holmes",novel.getAuthor());

    }


}
