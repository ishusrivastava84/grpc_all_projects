package Day3;

import Day3.Assignment.A6.Accounts;
import Day3.Assignment.A6.Customer;
import com.beust.ah.A;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class Assignment6Test {



    //constructor test for customer

    @Test
    public void customerConstructorTest(){
        Customer customer = new Customer("Nikhil","Anand");
        assertEquals("Nikhil",customer.getFirstName());
        assertEquals("Anand",customer.getLastName());
    }


    //constructor test for account getter and setter

    @Test
    public void customerAccountsGetterTest() {
        Customer customer = new Customer("Nikhil","Anand");
        customer.setAccounts(new Accounts(899.89));
        assertEquals(899.89,customer.getAccounts().getBalance(),0);
        assertNotEquals(28.90,customer.getAccounts().getBalance(),0);

    }

    //accounts deposit and withdraw function test

    @Test
    public void accountFunctionTest(){
        Customer customer = new Customer("Nikhil","Anand");
        customer.setAccounts(new Accounts(899.89));
        assertEquals(899.89,customer.getAccounts().getBalance(),0);
        customer.getAccounts().deposit(29.89);
        assertEquals(929.78,customer.getAccounts().getBalance(),0.1);

    }
}



