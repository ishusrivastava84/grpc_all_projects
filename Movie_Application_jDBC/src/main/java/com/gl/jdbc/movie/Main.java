package com.gl.jdbc.movie;
import com.gl.jdbc.movie.entity.Movie;
import com.gl.jdbc.movie.service.MovieService;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws SQLException {
        System.out.println("Welcome to Movie Menia");
        MovieService ms=new MovieService();

        ms.addMovie(new Movie());
        ms.getMovieByYear();
        ms.updateRevenue();
        ms.deleteMovieById();

        }



}