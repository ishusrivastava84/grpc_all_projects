package com.gl.student.exception;

public class StudentException extends Exception{

    public StudentException(String errorMessage){
        super(errorMessage);
    }

}
