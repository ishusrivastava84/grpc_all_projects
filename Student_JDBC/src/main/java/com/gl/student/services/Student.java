package com.gl.student.services;

public class Student {

    private String studentId;
    private String studentName;
    private String StudentAddress;
    private String StudentEmail;

    public Student() {

    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentAddress() {
        return StudentAddress;
    }

    public void setStudentAddress(String studentAddress) {
        StudentAddress = studentAddress;
    }

    public String getStudentEmail() {
        return StudentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        StudentEmail = studentEmail;
    }

    public Student(String studentId, String studentName, String studentAddress, String studentEmail) {
        this.studentId = studentId;
        this.studentName = studentName;
        StudentAddress = studentAddress;
        StudentEmail = studentEmail;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId='" + studentId + '\'' +
                ", studentName='" + studentName + '\'' +
                ", StudentAddress='" + StudentAddress + '\'' +
                ", StudentEmail='" + StudentEmail + '\'' +
                '}';
    }



}
