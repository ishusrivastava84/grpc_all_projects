package com.grpc.amazon;

import com.grpc.amazon.login.loginpage;
import com.grpc.amazon.signIn.CreateAccount;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ServerInterceptor;
import io.grpc.ServerInterceptors;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("Welcome to Amazone shopping app");

        //Server server = ServerBuilder.forPort(9090).addService(new loginpage()).build();
        Server server =ServerBuilder.forPort(9090).addService(ServerInterceptors.intercept(new CreateAccount())).addService(new loginpage()).build();

        server.start();
        System.out.println("Login is running on port no. "+server.getPort());
        System.out.println("Create Account is running on port no."+server.getPort());
        server.awaitTermination();

    }
}