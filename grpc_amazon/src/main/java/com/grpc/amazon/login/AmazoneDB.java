package com.grpc.amazon.login;

import com.grpc.amazon.Amazon;
import com.grpc.amazon.signIn.CreateProfile;
import io.grpc.stub.StreamObserver;

import java.sql.*;

public class AmazoneDB {

    private final String url="jdbc:postgresql://localhost:5432/amazone";
    private final String username="postgres";

    private final String password= "ishu123";

    private final Connection conn = DriverManager.getConnection(this.url, this.username, this.password);

    public AmazoneDB() throws SQLException {
    }

    private Statement stmt=conn.createStatement();

    public void loginData(Amazon.loginMessage request, Amazon.APIResponse.Builder responce, StreamObserver<Amazon.APIResponse> responseObserver) throws SQLException {
        System.out.println("Inside DB");
        String str= "select username from login where username='"+request.getUsername()+"'";
        ResultSet rs=stmt.executeQuery(str);
        if (rs.next()) {
                responce.setResponseMessage("Username present in database");
                responce.setResponseCode(100);
        } else {
                responce.setResponseMessage("Username not present in database");
                responce.setResponseCode(200);}

        String pswd="select password from login where password ='"+request.getPassword()+"'";
        ResultSet rs1=stmt.executeQuery(pswd);

        if(rs1.next()){
            responce.setResponseMessage("Password present in database");
            responce.setResponseCode(100);}
        else{
            responce.setResponseMessage("Password not present in database");
            responce.setResponseCode(200);}
        responseObserver.onNext(responce.build());
        responseObserver.onCompleted();}

    public void craeteUserAccount(CreateProfile.UserAccount request) throws SQLException {
        System.out.println("Inside User Account DB");
        StringBuilder string=new StringBuilder("Insert into useraccount(user_name,user_conatct_no,user_email,user_password,user_address,user_state,user_pincode,user_country)");
        string.append("Values('").append(request.getName()).append("',").
                append(request.getMobileNo()).append(",'").
                append(request.getEmail()).append("','")
                .append(request.getPassword()).append("','")
                .append(request.getAddress()).append("','")
                .append(request.getState()).append("',")
                .append(request.getPincode()).append(",'")
                .append(request.getCountry()).append("');");

        stmt.execute(string.toString());
        System.out.println("User Profile Created Succesfully");

        conn.close();

    }

    public void createBusinessAccount(CreateProfile.BusinessAccount request) throws SQLException {
        System.out.println("Inside Business Account DB");
        StringBuilder str=new StringBuilder("Insert into businessaccount(buss_name,buss_contact_no,buss_email,buss_password,buss_brand,buss_device,buss_specification,buss_price,buss_address,buss_country)");
        str.append("Values('").append(request.getName()).append("',")
                .append(request.getContactNo()).append(",'")
                .append(request.getBusinessEmail()).append("','")
                .append(request.getPassword()).append("','{")
                .append(request.getBrandList()).append("}','{")
                .append(request.getDevicesList()).append("}','")
                .append(request.getSpecification()).append("',")
                .append(request.getPrice()).append(",'")
                .append(request.getShopAddress()).append("','")
                .append(request.getCountry()).append("');");

        stmt.execute(str.toString());
        System.out.println("Business Account Created Sucessfully");

        conn.close();

        }
}
