package com.grpc.amazon.signIn;

import java.sql.SQLException;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: CreateProfile.proto")
public final class createProfileGrpc {

  private createProfileGrpc() {}

  public static final String SERVICE_NAME = "createProfile";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.grpc.amazon.signIn.CreateProfile.UserAccount,
      com.grpc.amazon.signIn.CreateProfile.APIResponse> getUserProfileMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "UserProfile",
      requestType = com.grpc.amazon.signIn.CreateProfile.UserAccount.class,
      responseType = com.grpc.amazon.signIn.CreateProfile.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.grpc.amazon.signIn.CreateProfile.UserAccount,
      com.grpc.amazon.signIn.CreateProfile.APIResponse> getUserProfileMethod() {
    io.grpc.MethodDescriptor<com.grpc.amazon.signIn.CreateProfile.UserAccount, com.grpc.amazon.signIn.CreateProfile.APIResponse> getUserProfileMethod;
    if ((getUserProfileMethod = createProfileGrpc.getUserProfileMethod) == null) {
      synchronized (createProfileGrpc.class) {
        if ((getUserProfileMethod = createProfileGrpc.getUserProfileMethod) == null) {
          createProfileGrpc.getUserProfileMethod = getUserProfileMethod = 
              io.grpc.MethodDescriptor.<com.grpc.amazon.signIn.CreateProfile.UserAccount, com.grpc.amazon.signIn.CreateProfile.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "createProfile", "UserProfile"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.grpc.amazon.signIn.CreateProfile.UserAccount.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.grpc.amazon.signIn.CreateProfile.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new createProfileMethodDescriptorSupplier("UserProfile"))
                  .build();
          }
        }
     }
     return getUserProfileMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.grpc.amazon.signIn.CreateProfile.BusinessAccount,
      com.grpc.amazon.signIn.CreateProfile.APIResponse> getBusinessProfileMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "BusinessProfile",
      requestType = com.grpc.amazon.signIn.CreateProfile.BusinessAccount.class,
      responseType = com.grpc.amazon.signIn.CreateProfile.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.grpc.amazon.signIn.CreateProfile.BusinessAccount,
      com.grpc.amazon.signIn.CreateProfile.APIResponse> getBusinessProfileMethod() {
    io.grpc.MethodDescriptor<com.grpc.amazon.signIn.CreateProfile.BusinessAccount, com.grpc.amazon.signIn.CreateProfile.APIResponse> getBusinessProfileMethod;
    if ((getBusinessProfileMethod = createProfileGrpc.getBusinessProfileMethod) == null) {
      synchronized (createProfileGrpc.class) {
        if ((getBusinessProfileMethod = createProfileGrpc.getBusinessProfileMethod) == null) {
          createProfileGrpc.getBusinessProfileMethod = getBusinessProfileMethod = 
              io.grpc.MethodDescriptor.<com.grpc.amazon.signIn.CreateProfile.BusinessAccount, com.grpc.amazon.signIn.CreateProfile.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "createProfile", "BusinessProfile"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.grpc.amazon.signIn.CreateProfile.BusinessAccount.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.grpc.amazon.signIn.CreateProfile.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new createProfileMethodDescriptorSupplier("BusinessProfile"))
                  .build();
          }
        }
     }
     return getBusinessProfileMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static createProfileStub newStub(io.grpc.Channel channel) {
    return new createProfileStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static createProfileBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new createProfileBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static createProfileFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new createProfileFutureStub(channel);
  }

  /**
   */
  public static abstract class createProfileImplBase implements io.grpc.BindableService {

    /**
     */
    public void userProfile(com.grpc.amazon.signIn.CreateProfile.UserAccount request,
        io.grpc.stub.StreamObserver<com.grpc.amazon.signIn.CreateProfile.APIResponse> responseObserver) throws SQLException {
      asyncUnimplementedUnaryCall(getUserProfileMethod(), responseObserver);
    }

    /**
     */
    public void businessProfile(com.grpc.amazon.signIn.CreateProfile.BusinessAccount request,
        io.grpc.stub.StreamObserver<com.grpc.amazon.signIn.CreateProfile.APIResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getBusinessProfileMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getUserProfileMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.grpc.amazon.signIn.CreateProfile.UserAccount,
                com.grpc.amazon.signIn.CreateProfile.APIResponse>(
                  this, METHODID_USER_PROFILE)))
          .addMethod(
            getBusinessProfileMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.grpc.amazon.signIn.CreateProfile.BusinessAccount,
                com.grpc.amazon.signIn.CreateProfile.APIResponse>(
                  this, METHODID_BUSINESS_PROFILE)))
          .build();
    }
  }

  /**
   */
  public static final class createProfileStub extends io.grpc.stub.AbstractStub<createProfileStub> {
    private createProfileStub(io.grpc.Channel channel) {
      super(channel);
    }

    private createProfileStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected createProfileStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new createProfileStub(channel, callOptions);
    }

    /**
     */
    public void userProfile(com.grpc.amazon.signIn.CreateProfile.UserAccount request,
        io.grpc.stub.StreamObserver<com.grpc.amazon.signIn.CreateProfile.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUserProfileMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void businessProfile(com.grpc.amazon.signIn.CreateProfile.BusinessAccount request,
        io.grpc.stub.StreamObserver<com.grpc.amazon.signIn.CreateProfile.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getBusinessProfileMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class createProfileBlockingStub extends io.grpc.stub.AbstractStub<createProfileBlockingStub> {
    private createProfileBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private createProfileBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected createProfileBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new createProfileBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.grpc.amazon.signIn.CreateProfile.APIResponse userProfile(com.grpc.amazon.signIn.CreateProfile.UserAccount request) {
      return blockingUnaryCall(
          getChannel(), getUserProfileMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.grpc.amazon.signIn.CreateProfile.APIResponse businessProfile(com.grpc.amazon.signIn.CreateProfile.BusinessAccount request) {
      return blockingUnaryCall(
          getChannel(), getBusinessProfileMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class createProfileFutureStub extends io.grpc.stub.AbstractStub<createProfileFutureStub> {
    private createProfileFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private createProfileFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected createProfileFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new createProfileFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.grpc.amazon.signIn.CreateProfile.APIResponse> userProfile(
        com.grpc.amazon.signIn.CreateProfile.UserAccount request) {
      return futureUnaryCall(
          getChannel().newCall(getUserProfileMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.grpc.amazon.signIn.CreateProfile.APIResponse> businessProfile(
        com.grpc.amazon.signIn.CreateProfile.BusinessAccount request) {
      return futureUnaryCall(
          getChannel().newCall(getBusinessProfileMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_USER_PROFILE = 0;
  private static final int METHODID_BUSINESS_PROFILE = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final createProfileImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(createProfileImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_USER_PROFILE:
          try {
            serviceImpl.userProfile((CreateProfile.UserAccount) request,
                (io.grpc.stub.StreamObserver<CreateProfile.APIResponse>) responseObserver);
          } catch (SQLException e) {
            throw new RuntimeException(e);
          }
          break;
        case METHODID_BUSINESS_PROFILE:
          serviceImpl.businessProfile((com.grpc.amazon.signIn.CreateProfile.BusinessAccount) request,
              (io.grpc.stub.StreamObserver<com.grpc.amazon.signIn.CreateProfile.APIResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class createProfileBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    createProfileBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.grpc.amazon.signIn.CreateProfile.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("createProfile");
    }
  }

  private static final class createProfileFileDescriptorSupplier
      extends createProfileBaseDescriptorSupplier {
    createProfileFileDescriptorSupplier() {}
  }

  private static final class createProfileMethodDescriptorSupplier
      extends createProfileBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    createProfileMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (createProfileGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new createProfileFileDescriptorSupplier())
              .addMethod(getUserProfileMethod())
              .addMethod(getBusinessProfileMethod())
              .build();
        }
      }
    }
    return result;
  }
}
